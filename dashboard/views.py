from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    '''
        input: an HTTP(S) request
        output: a rendered page
    '''
    template = 'dashboard/index.html'
    return render(request, template)
